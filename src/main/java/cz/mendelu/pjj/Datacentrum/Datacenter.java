package cz.mendelu.pjj.Datacentrum;


import java.io.Serializable;
import java.util.ArrayList;

import static cz.mendelu.pjj.Datacentrum.Type.*;

public class Datacenter implements Serializable {

	private ArrayList<Block> blocks;
	private Customer[] customers;
	private String name;
	private int floors;
	private static Datacenter datacenter = new Datacenter();
	private int money;

	public void setMoney(int money) {
		this.money = money;
	}

	public int getMoney() {

		return money;
	}

	private Datacenter() {
		this.money = 6000;
		Element el = new Element("ad",SKY,400,5);
		blocks = new ArrayList<Block>();

		for (int i=0; i < 100; i++){
			this.blocks.add(new Block(new Element("sky",SKY, 0,0)));
		}
		this.blocks.set(95, new Block(new Element("room",BUILDING, 0,0)));
		this.blocks.set(94, new Block(new Element("room",BUILDING, 0,0)));
		this.blocks.set(93, new Block(new Element("room",BUILDING, 0,0)));

		this.blocks.set(85, new Block(new Element("room",BUILDING, 0,0)));
		this.blocks.set(84, new Block(new Element("room",BUILDING, 0,0)));
		this.blocks.set(83, new Block(new Element("room",BUILDING, 0,0)));

		this.blocks.set(75, new Block(new Element("room",BUILDING, 0,0)));
		this.blocks.set(74, new Block(new Element("room",BUILDING, 0,0)));
		this.blocks.set(73, new Block(new Element("room",BUILDING, 0,0)));

		this.blocks.set(90, new Block(new Element("room",GROUND, 0,0)));
		this.blocks.set(91, new Block(new Element("room",GROUND, 0,0)));
		this.blocks.set(92, new Block(new Element("room",GROUND, 0,0)));

		this.blocks.set(96, new Block(new Element("room",GROUND, 0,0)));
		this.blocks.set(97, new Block(new Element("room",GROUND, 0,0)));
		this.blocks.set(98, new Block(new Element("room",GROUND, 0,0)));
		this.blocks.set(99, new Block(new Element("room",GROUND, 0,0)));


	}

	/**
	 * 
	 * @param name
	 * @param floors
	 */
//	public Datacenter generateDatacenter(String name, int floors) {

//	}

	public static Datacenter getDatacenter(){
		return datacenter;
	}

	/**
	 * 
	 * @param block
	 */
	public ArrayList<Block> getBlocks() {

		return this.blocks;
	}

	public Customer getCustomer() {
		// TODO - implement Datacenter.getCustomer
		throw new UnsupportedOperationException();
	}

	public String getAllStats() {
		int servers = 0;
		int switches = 0;
		int staff = 0;

		for (Block i : this.blocks) {
			if(i.getElement().getType() == SERVER) servers++;
			if(i.getElement().getType() == SWITCH) switches++;
			if(i.getElement().getType() == STAFF) staff++;
		}
		return "Servers: "+servers+" Switches: "+switches+" Staff: "+staff;
	}


	public void buy(String type, int kde, int money, int earn){
		if(type.equals("building") &&
				getBlocks().get(kde+10).getElement().getType() != SKY
				&& getBlocks().get(kde).getElement().getType() == SKY){
			getBlocks().get(kde).getElement().setType(type);
			setMoney(this.money - money);
		}

		if(!type.equals("building") && getBlocks().get(kde).getElement().getType() == BUILDING){
			getBlocks().get(kde).getElement().setType(type);
			getBlocks().get(kde).getElement().setEarn(earn);
			setMoney(this.money - money);
		}

	}


	public void recalculationBalance(){
		int total = 0;
		for (Block i : this.blocks) {
			total = total + i.getElement().getEarn();
		}
		System.out.println(total);
		this.money = money + total;
	}


}