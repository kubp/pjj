package cz.mendelu.pjj.Datacentrum;

import java.io.Serializable;

enum Type {
	GROUND("sky"), SKY("sky"), BUILDING("building"), SERVER("server"), SWITCH("switch"), STAFF("staff");

	private String type;

	private Type(String type) {
		this.type = type;
	}

	public String getType(){
		return type;
	}

	}

public class Element implements Serializable {

	private String name;
	private Type type;
	private int price;
	private int earn;

	public void setEarn(int earn) {
		this.earn = earn;
	}

	public int getEarn() {

		return earn;
	}

	/**
	 * 
	 * @param name
	 * @param type
	 * @param price
	 */
	public Element(String name, Type type, int price, int earn) {
		this.type = type;
		this.price = price;
		this.earn = earn;
	}

	public StatsStruct getStats() {
		return null;
	}

	public String getName() {
		return this.name;
	}

	public Type getType() {
		return this.type;
	}

	public String getImage(){
		switch (this.type) {
			case SKY:
				return "sky.png";
			case BUILDING:
				return "building.png";
			case SWITCH:
				return "switch.png";
			case STAFF:
				return "staff.png";
			case SERVER:
				return "server.png";
			case GROUND:
				return "ground.png";
			default:
				return "ad";
		}
	}

	public void setType(String type){
		switch (type) {
			case "server":
				this.type = Type.SERVER;
				break;

			case "staff":
				this.type = Type.STAFF;
				break;
			case "building":
				this.type = Type.BUILDING;
				break;
			case "switch":
				this.type = Type.SWITCH;
				break;

		}
	}

	public int getPrice() {
		return this.price;
	}

}