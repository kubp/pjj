package cz.mendelu.pjj.Datacentrum;

import java.io.Serializable;

public class Block implements Serializable {

	private Element element;


	public Block(Element element) {
		this.element = element;
	}

	Block() {
		this.element = null;
	}


	public Element getElement() {
		return this.element;
	}



	/**
	 * 
	 * @param element
	 */
	public void setElement(Element element) {
		this.element = element;
	}

	public boolean isEmpty() {
		return true;
	}

}