package cz.mendelu.pjj.Datacentrum;

public class Customer {

	private int pays;
	private int hasCores;
	private int hasMemory;
	private int hasHdd;
	private boolean isCustomer;

	/**
	 *
	 * @param pays
	 * @param hasCores
	 * @param hasMemory
	 * @param hasHdd
	 */

	public Customer(int pays, int hasCores, int hasMemory, int hasHdd, boolean isCustomer) {
		this.pays = pays;
		this.hasCores = hasCores;
		this.hasMemory = hasMemory;
		this.hasHdd = hasHdd;
		this.isCustomer = isCustomer;
	}


	public int getPays() {
		return this.pays;
	}

	public int getHasCores() {
		return this.hasCores;
	}

	public int getHasMemory() {
		return this.hasMemory;
	}

	public int getHashHdd() {
		return this.hasHdd;
	}

}