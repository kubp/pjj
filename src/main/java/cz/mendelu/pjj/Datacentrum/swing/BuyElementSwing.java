package cz.mendelu.pjj.Datacentrum.swing;

import cz.mendelu.pjj.Datacentrum.Datacenter;
import greenfoot.Block;
import greenfoot.BuyElement;

import javax.swing.*;
import java.awt.*;

public class BuyElementSwing extends JFrame {
    public BuyElementSwing(int cord, Datacenter data) throws HeadlessException {
        greenfoot.BuyElement settings = new greenfoot.BuyElement(cord, data);
        this.setContentPane(settings.getRootPanel());
        this.pack();
    }
}
