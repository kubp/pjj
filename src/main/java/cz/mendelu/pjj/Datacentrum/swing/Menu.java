package cz.mendelu.pjj.Datacentrum.swing;

import cz.mendelu.pjj.Datacentrum.Datacenter;
import greenfoot.Greenfoot;
import greenfoot.MyWorld;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class Menu {
    private JFrame frame;

    private JButton nacistButton;
    private JButton ulozitButton;
    private JButton exitButton;
    private JPanel panel;
    private JLabel text;
    private Datacenter data;

    public Menu(Datacenter data) {
        this.data = data;

        this.frame = new JFrame("Datacenter");
        this.frame.setContentPane(panel);
        frame.setSize(500, 300);
        this.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Greenfoot.start();
            }
        });

        nacistButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("datacenter.bin"))) {
                    Datacenter data = (Datacenter) ois.readObject();
                    MyWorld robotWorld = new MyWorld(data);
                    Greenfoot.setWorld(robotWorld);
                    Greenfoot.start();
                    frame.dispose();

                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        });

        ulozitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("datacenter.bin"))) {

                    oos.writeObject(data);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });


        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });


    }

    public void open() {
        SwingUtilities.invokeLater(() -> {
            frame.setVisible(true);
        });
    }

    private void saveDatacenter(){

    }

    private void loadDatacenter(){

    }

}
