package cz.mendelu.pjj.Datacentrum;

public class Pyroman {
    private static Pyroman ourInstance = new Pyroman();

    public static Pyroman getInstance() {
        return ourInstance;
    }

    private Timer timer;

    private Pyroman() {
        this.timer = new Timer();
    }

    public Timer getTimer() {
        return timer;
    }
}
