package cz.mendelu.pjj.Datacentrum;

import java.util.ArrayList;
import java.util.List;

public class Timer {

    public interface OnTimeListener{
        void onTime(Timer timer, int currentTime);

    }

    private static class TimeEvent{
        private final OnTimeListener onTimeListener;
        private int time;

        private TimeEvent(OnTimeListener onTimeListener, int time) {
            this.onTimeListener = onTimeListener;
            this.time = time;
        }
    }

    private int currentTime = 0;

    private List<TimeEvent> timeEventList = new ArrayList<>();

    public void incrementTime(){
        currentTime++;
        for (TimeEvent event : timeEventList) {
            if ((currentTime % event.time) == 0){
                event.onTimeListener.onTime(this, currentTime);
            }
        }
    }

    public int getCurrentTime(){
        return currentTime/60;
    }

    public void addOnTimeListener(OnTimeListener listener, int time){
        timeEventList.add(new TimeEvent(listener, currentTime+time));
    }
}
