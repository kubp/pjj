package greenfoot;

import cz.mendelu.pjj.Datacentrum.Datacenter;
import cz.mendelu.pjj.Datacentrum.Pyroman;
import cz.mendelu.pjj.Datacentrum.swing.BuyElementSwing;
import cz.mendelu.pjj.Datacentrum.Bomba;
import cz.mendelu.pjj.Datacentrum.Timer;
import cz.mendelu.pjj.Datacentrum.swing.Menu;

import javax.swing.*;
import javax.xml.crypto.Data;

public class MyWorld extends World {
    /**
     * Create a new world with 8x8 cells and
     * with a cell size of 60x60 pixels
     */
    private Timer timer = Pyroman.getInstance().getTimer();
    private Bomba bomb = new Bomba();
    private Datacenter data = Datacenter.getDatacenter();

    // nothing do, just for loading
    public MyWorld() {
        super(10, 10, 90);
        this.setBackground("images/world-background.png");

        this.data = data;
        int f= 0;
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                addObject(
                        new greenfoot.Block(f, data),
                        j,
                        i
                );
                f++;
            }
        }


        addObject(new StatsText(data),2,-1);

        Menu menu = new Menu(data);
        menu.open();

    }


    public MyWorld(Datacenter data) {
        super(10, 10, 90);
        this.setBackground("images/world-background.png");

        this.data = data;
        int f= 0;
        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){
                addObject(
                        new greenfoot.Block(f, data),
                        j,
                        i
                );
                f++;
            }
        }

        addObject(new StatsText(data),2,-1);
        addObject(new StatsMoney(data),5,-1);
        addObject(new TimerActor(timer), 7, -1);


        Menu menu = new Menu(data);
        menu.open();



            timer.addOnTimeListener(new Timer.OnTimeListener() {
             @Override
            public void onTime(Timer timer, int currentTime) {
                 data.recalculationBalance();
          }
        },100);
    }

    @Override
    public void act() {
        String key = Greenfoot.getKey();
        if("s".equals(key)){
            Greenfoot.stop();



        }
    }

}