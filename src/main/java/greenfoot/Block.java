package greenfoot;

import cz.mendelu.pjj.Datacentrum.Datacenter;
import cz.mendelu.pjj.Datacentrum.swing.BuyElementSwing;

import javax.swing.*;


public class Block extends Actor {
    private cz.mendelu.pjj.Datacentrum.Block block;
    private int cord;
    private Datacenter data;

    public Block(int cord, Datacenter data) {
        this.block = data.getBlocks().get(cord);
        this.cord = cord;
        this.data = data;

        setImage("images/"+block.getElement().getImage());
    }

    @Override
    public void act() {
        setImage("images/"+block.getElement().getImage());
        if (Greenfoot.mouseClicked(this)) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    BuyElementSwing frame = new BuyElementSwing(cord, data);
                    frame.setTitle("Buy");
                    frame.setVisible(true);
                }
            });
        }
        // String key = Greenfoot.getKey();
       // move(3);
    }
}
