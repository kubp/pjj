package greenfoot;
import cz.mendelu.pjj.Datacentrum.Timer;

import java.awt.*;

public class TimerActor extends Actor {
    private Timer timer;

    public TimerActor(Timer timer){
        this.timer=timer;
        this.updateLabel();
    }

    @Override
    public void act() {
        timer.incrementTime();
        updateLabel();
    }

    private void updateLabel(){
        GreenfootImage gfi = new GreenfootImage(
                "Time "+ timer.getCurrentTime(),
                20,
                new Color(255,255,255),
                new Color(0, 43, 12, 40));
        setImage(gfi);
    }
}
