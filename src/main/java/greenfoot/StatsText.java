package greenfoot;
import cz.mendelu.pjj.Datacentrum.Datacenter;
import cz.mendelu.pjj.Datacentrum.Timer;

import java.awt.*;

public class StatsText extends Actor {
    private Timer timer;


    private Datacenter d;
    public StatsText(Datacenter d){
        this.d = d;

    }

    @Override
    public void act() {


        updateLabel(this.d.getAllStats());
    }

    private void updateLabel(String stats){
        GreenfootImage gfi = new GreenfootImage(
                stats,
                20,
                new Color(255,255,255),
                new Color(0, 43, 12, 40));
        setImage(gfi);
    }
}
