package greenfoot;
import cz.mendelu.pjj.Datacentrum.Datacenter;
import cz.mendelu.pjj.Datacentrum.Timer;

import java.awt.*;

public class StatsMoney extends Actor {
    private Timer timer;


    private Datacenter d;
    public StatsMoney(Datacenter d){
        this.d = d;

    }

    @Override
    public void act() {


        updateLabel("Money: $" + Integer.toString(this.d.getMoney()));
    }

    private void updateLabel(String stats){
        GreenfootImage gfi = new GreenfootImage(
                stats,
                20,
                new Color(255,255,255),
                new Color(0, 43, 12, 40));
        setImage(gfi);
    }
}
