package greenfoot;

import cz.mendelu.pjj.Datacentrum.Block;
import cz.mendelu.pjj.Datacentrum.Datacenter;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class BuyElement {
    private JPanel rootPanel;
    private JTextField textField1;
    private JButton přidatButton;
    private JButton koupitServerButton;
    private JButton koupitSwitchButton;
    private JButton koupitStaffButton;
    private JButton koupitBuildingButton;
    private JTextField textField2;

    public BuyElement(int cord, Datacenter data) {
        přidatButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        koupitSwitchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("asasas");
                int money = Integer.parseInt(textField1.getText());
                int earn = Integer.parseInt(textField2.getText());
                data.buy("switch", cord, money, earn);
            }
        });

        koupitServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int money = Integer.parseInt(textField1.getText());
                int earn = Integer.parseInt(textField2.getText());
                data.buy("server", cord, money, earn);
            }
        });

        koupitBuildingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int money = Integer.parseInt(textField1.getText());
                int earn = Integer.parseInt(textField2.getText());
                data.buy("building", cord, money, earn);
            }
        });

        koupitStaffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int money = Integer.parseInt(textField1.getText());
                int earn = Integer.parseInt(textField2.getText());
                data.buy("staff", cord, money, earn);
            }
        });
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }
}
